package xray

import (
	"context"
	"net/http"
)

type PolicyService Service

type Policy struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	Rules 		[]Rule `json:"rules"`
	Description string    `json:"description,omitempty"`
}

type Rule struct {
	Name     string `json:"name"`
	Priority int    `json:"priority"`
	Criteria Criteria `json:"criteria"`
	Actions  Action `json:"actions"`
}

type Criteria struct {
	MinSeverity string `json:"min_severity"`
}

type Action struct {
	Mails         []string `json:"mails"`
	FailBuild     bool     `json:"fail_build"`
	BlockDownload BlockDownload `json:"block_download"`
}

type BlockDownload struct {
	Unscanned bool `json:"unscanned"`
	Active    bool `json:"active"`
}

type PolicyListOptions struct {
	Type string `url:"type,omitempty"`
}


func (s *PolicyService) CreatePolicies(ctx context.Context, policy *Policy) (*http.Response, error){
	return s.create(ctx, policy)
}

func (s *PolicyService) create(ctx context.Context, v interface{}) (*http.Response, error) {
	path := "api/v1/policies"
	req, err := s.Client.NewJSONEncodedRequest(http.MethodPost, path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}

func (s *PolicyService) UpdatePolicies(ctx context.Context, policy *Policy, nameOfPolicy string) (*http.Response, error){
	return s.update(ctx, policy, nameOfPolicy)
}

func (s *PolicyService) update(ctx context.Context, v interface{}, nameOfPolicy string) (*http.Response, error) {
	path := "api/v1/policies/" + nameOfPolicy
	req, err := s.Client.NewJSONEncodedRequest(http.MethodPut, path, v)
	if err != nil {
		return nil, err
	}

	return s.Client.Do(ctx, req, nil)
}
