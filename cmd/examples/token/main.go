package main

import (
	"context"
	"fmt"
	"gitlab.com/aurelien.gabet/go-xray-cli/pkg/types"
	"gitlab.com/aurelien.gabet/go-xray-cli/pkg/xray"
	"net/http"
	"os"
	"time"
)

func createPolicy(client *xray.Client, policy *xray.Policy) (*http.Response, error){

	resp, err := client.Policies.CreatePolicies(context.Background(), policy)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}

func updatePolicy(client *xray.Client, policy *xray.Policy, name string) {

	resp, err := client.Policies.UpdatePolicies(context.Background(), policy, name)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
}

func createWatch(client *xray.Client, watch *xray.Watch) (*http.Response, error) {

	resp, err := client.Watches.CreateWatch(context.Background(), watch)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}

func updateWatch(client *xray.Client, watch *xray.Watch, name string) (*http.Response, error) {

	resp, err := client.Watches.UpdateWatch(context.Background(), watch, name)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
	return resp, err
}


func main() {

	/*	tp := xray.TokenAuthTransport{
			Token: fmt.Sprintf("Bearer " + os.Getenv("ARTIFACTORY_TOKEN")),
		}*/

	credentials := types.GetToken{
		Name: os.Getenv("ADMIN"),
		Password: os.Getenv("PASSWORD"),
	}

	client, err := xray.NewClientWithToken(os.Getenv("XRAY_URL"), &credentials)
	if err != nil {
		fmt.Printf("\nerror: %v\n", err)
	}

	/*	tp := xray.TokenAuthTransport{
			Token: fmt.Sprintf("Bearer " + token),
		}

		client, err := xray.NewClient(os.Getenv("XRAY_URL"), tp.Client())
		if err != nil {
			fmt.Printf("\nerror: %v\n", err)
		}*/
	/*
		client, err := xray.NewClient(os.Getenv("XRAY_URL"), tp.Client())
		if err != nil {
			fmt.Printf("\nerror: %v\n", err)
			return
		}*/

	policyToCreate := &xray.Policy{
		Name: "aug-e4-docker-scratch-intranet",
		Type: "security",
		Rules: []xray.Rule{xray.Rule{
			Name:     "aug-e4-docker-scratch-intranet",
			Priority: 1,
			Criteria: xray.Criteria{
				MinSeverity: "High",
			},
			Actions: xray.Action{
				Mails:     []string{"aurelien.gabet@example.com"},
				FailBuild: false,
				BlockDownload: xray.BlockDownload{
					Unscanned: false,
					Active:    false,
				},
			}},
		},
		Description: "aug test",
	}

	for i := 0 ; i < 100 ; i++ {
		resp, _ := createPolicy(client, policyToCreate)
		fmt.Println(resp)
		time.Sleep(5 * time.Second)
	}

}
